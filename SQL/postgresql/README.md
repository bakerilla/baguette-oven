SQL Cheatsheet.
Currently covers both postgres and mysql

## Postgres ##

### Getting Started  ###

#### Docker ####
#### Setting Up `libpq` 
##### macos
1. install homebrew
2. run 
    
    ```brew install libpq```
3. update PATH

    if using zsh:

    ```
    echo 'export PATH="/usr/local/opt/libpq/bin:$PATH"' >> ~/.zshrc
    source ~/.zshrc
    ```

    if using bash:

    ```
    echo 'export PATH="/usr/local/opt/libpq/bin:$PATH"' >> ~/.bash_profile
    source ~/.bash_profile
    ```

        
### Indexes  ###
### Triggers  ###

[MySQL](mysql/README.md)
